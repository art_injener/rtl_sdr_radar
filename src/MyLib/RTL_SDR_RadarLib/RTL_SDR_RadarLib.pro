TEMPLATE = subdirs

SUBDIRS += \
    Subject \
    Carrier \
    MapLib \
    PoolObject \
    GraphicsWidget \
    Logger

CONFIG += ordered
